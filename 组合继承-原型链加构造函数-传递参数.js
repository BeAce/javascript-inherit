(function() {
  function Father(name) {
    this.name = name;
    this.color = ["yellow", "red"];
  }

  Father.prototype.sayName = function() {
    console.log("也可以复用父类方法 father sayname", this.name);
  };

  function Son(name, age) {
    Father.call(this, name);
    this.age = age;
  }

  Son.prototype = new Father();

  Son.prototype.sayAge = function() {
    console.log("son sayage", this.age);
  };

  console.group("组合继承，传递参数， 调用了两次父类构造函数，引起不必要的内存消耗 Father.call/new Father");
  const son = new Son("blue", 18);
  son.color.push("black");
  console.log(`son.color.push('black')`, son.color);
  son.sayName();
  son.sayAge();
  console.groupEnd();
})();
