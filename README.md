# javascript-inherit

JavaScript 继承相关知识点总结

## 继承都离不开这两个变体

- 构造函数继承

```js
function Parent() {
  this.name = "parent";
}
function Son() {
  Parent.call(this);
}
const son = new Son();
console.log(son.name) // parent
```

- 原型继承
```js
function Parent() {}
Parent.prototype.name = "parent";
function Son() {}
Son.prototype = new Parent;
const son = new Son();
console.log(son.name) // parent;
```

## 上述方式整合
```js
function Son() {
  Parent.call(this);
}
Son.prototype = new Parent;
Son.prototype.newProp = function() {
  // your code
}
```

### 优化一些内存
```js
function Son() {
  Parent.call(this);
  this.name = "son";
}
(function() {
  const Super = function() {};
  Super.prototype = new Parent;
  Son.protoType = new Super;
  Son.prototype.newProp = function() {
    // your code
  }
})()
```
