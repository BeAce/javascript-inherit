(function() {
  function Father() {
    this.property = true;
  }

  Father.prototype.getFatherValue = function() {
    return this.property;
  };

  function Son() {
    this.sonProperty = false;
  }

  Son.prototype = new Father();
  Son.prototype.getSonValue = function() {
    return this.sonProperty;
  };

  const son = new Son();
  console.group("Son 继承 Father");
  console.log(son);
  console.log("son.getFatherValue()", son.getFatherValue());
  // son 的构造函数指向 Father
  console.log("son.constructor === Father", son.constructor === Father);
  console.groupEnd();

  console.group("确定继承关系");
  // 如何确定继承关系
  console.log("son instanceof Object", son instanceof Object);
  console.log("son instanceof Father", son instanceof Father);
  console.log("son instanceof Son", son instanceof Son);

  console.log(
    "Object.prototype.isPrototypeOf(son)",
    Object.prototype.isPrototypeOf(son)
  );
  console.log(
    "Father.prototype.isPrototypeOf(son)",
    Father.prototype.isPrototypeOf(son)
  );
  console.log(
    "Son.prototype.isPrototypeOf(son)",
    Son.prototype.isPrototypeOf(son)
  );

  console.groupEnd();
})();
