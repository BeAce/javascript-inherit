(function() {
  console.group("object 对传入对象进行了一次浅复制， 多个实例是共享的");
  function object(o) {
    function F() {}
    F.prototype = o;
    return new F();
  }

  const person = {
    friends: ["Beace", "Jerry"]
  };
  const p = object(person);
  p.friends.push("Allens");
  const p2 = object(person);

  p2.friends.push("Bob");

  console.log(`p.friends.push('Allens'); p2.friends.push('Bob');`, p.friends);
  console.groupEnd();

  console.group('Object.create() 方法规范了上面的继承');
  const p3 = Object.create(person);
  p3.friends.push('Amy');
  const p4 = Object.create(person);
  p4.friends.push('Blue');
  console.log('Object.create(person) push Amy and blue', person.friends);
  console.groupEnd();
})();
