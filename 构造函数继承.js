(function() {
  // 父类中定义的方法，对子类是不可见的
  function Father() {
    this.colors = ["red", "yellow"];
    this.hello = function() {
      console.log("方法只能在构造函数中定义子类才能复用 say hello");
    };
  }
  // 父类方法对子类不可见
  Father.prototype.sayHi = function() {
    console.log("father say hi");
  };

  function Son() {
    Father.call(this);
  }

  const son = new Son();
  son.colors.push("blue");
  console.group("构造函数方式 Son 继承 Father");
  console.log("son", son);
  console.log("son colors", son.colors);
  console.groupEnd();

  try {
    son.sayHi();
  } catch (error) {
    console.warn("son.sayHi() 父类方法对子类不可见, 无法函数复用", error);
  }

  son.hello();

  const son2 = new Son();
  console.group("多个实例互不影响");
  console.log("son2", son2);
  console.groupEnd();
})();
